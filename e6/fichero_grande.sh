#!bin/bash

NUM_LINEAS=`cat $1 | wc -l`

if [ $NUM_LINEAS -le 10 ]; then
  echo 'PEQUEÑO'
else
  echo 'GRANDE'
fi

echo $NUM_LINEAS
