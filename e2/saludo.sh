#!bin/bash

echo "Número de parámetros = $#"

if [ $# -le 0 ]; then
  echo "Tienes que decirme tu nombre!"
  exit 1
fi

echo "Hola $@!"
