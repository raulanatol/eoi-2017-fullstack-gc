#!bin/bash

function help() {
  cat << EOF
USO
  $0 NOMBRE_1 [NOMBRE_2] ... [NOMBRE_N]

DESCRIPCION
  Saludamos a todos los nombres indicados por parámetros

CÓDIGOS DE EXIT
  1 Si el número de parámetros es menor que 1
EOF
}

if [ $# -le 0 ]; then
  echo "Tienes que decirme tu nombre!"
  help
  exit 1
fi

MSG="Hola"
FIRST=1

while [ -n "$1" ]; do
  if [ $FIRST -eq 1 ]; then
    MSG="$MSG $1"
    FIRST=0
  else
    MSG="$MSG, $1"
  fi

  # Siguiente parámetro
  shift
done
echo $MSG



exit 0
