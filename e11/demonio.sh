#! /bin/bash

# demonio para homer-alerta
DAEMON=homer-alarm
PIDFILE=/tmp/$DAEMON.pid

function do_start() {
  if [ -e $PIDFILE ]; then
    echo "El proceso ya se está ejecutando."
    exit 0;
  fi
  ./$DAEMON &
  echo $! > $PIDFILE
  echo "Homer alert ON!"
}

function do_stop() {
  if [ -e $PIDFILE ]; then
    kill -9 `cat $PIDFILE`
    rm $PIDFILE
  fi
  echo "Homer alert OFF!"
}

function do_restart() {
  do_stop
  do_start
}

function do_status() {
  if [ -e $PIDFILE ]; then
    PIDVALUE=`cat $PIDFILE`
    echo "Ejecutandose con $PIDVALUE"
  else
    echo "Parado."
  fi
}

case $1 in
    start)
      do_start ;;
    stop)
      do_stop ;;
    restart)
      do_restart ;;
    status)
      do_status ;;
    *)
      echo "Parámetro '$1' incorrecto." ;;
esac
