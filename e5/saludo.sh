#!bin/bash

function help() {
  cat << EOF
USO
  $0 NOMBRE_1 [NOMBRE_2] ... [NOMBRE_N]

DESCRIPCION
  Saludamos a todos los nombres indicados por parámetros

CÓDIGOS DE EXIT
  1 Si el número de parámetros es menor que 1
EOF
}

if [ $# -le 0 ]; then
  echo "Tienes que decirme tu nombre!"
  help
  exit 1
fi

while [ -n "$1" ]; do
  echo "Hola $1"
  shift
done

exit 0
